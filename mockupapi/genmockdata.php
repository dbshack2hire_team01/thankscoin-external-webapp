<?php
/*
 * Generate mock data for dashboard
 *
 * Accounts have a probability for sending and receiving
 *
 * Amounts also distributed probabilistically
 */

$desclist = file('nounlist.txt');

$ACC_CNT = 40;
$OPEN_PROB = 0.1;

$PROB_REC = [
  [
      "prob" => 0.05,
      "recvprob" => 1.8, // fake probability density for receiving money
      "sendprob" => 0.02 // probability of sending money at any given time
  ],
  [
      "prob" => 0.25,
      "recvprob" => 0.2,
      "sendprob" => 0.04,
  ],
  [
      "prob" => 0.71,
      "recvprob" => 0.01,
      "sendprob" => 0.1
   ]
];

function random() {
    return mt_rand() / mt_getrandmax();
}

function saveAccounts() {
    global $accounts;
    $d = [];
    foreach($accounts as $acc) {
        $d[] = [
            "account_number" => $acc['id'],
            "balance" => $acc['balance'],
            "open" => $acc['open'],
            "created" => $acc['created'],
            "last_transaction" => $acc['lasttx']
        ];
    }
    file_put_contents("/tmp/accounts.json", json_encode($d));
}

$accounts = [];
for ($i = 0; $i < $ACC_CNT; $i++) {
    $p = random();
    $j = 0;
    while ($p > $PROB_REC[$j]['prob']) {
        $p -= $PROB_REC[$j]['prob'];
        $j++;
    }

    $accounts[] = [
        "id" => md5(rand()),
        "balance" => rand(500, 10000),
        "created" => date('c'),
        "lasttx" => date('c'),
        "open" => random() < $OPEN_PROB,
        "recvprob" => $PROB_REC[$j]["recvprob"],
        "sendprob" => $PROB_REC[$j]["sendprob"]
    ];
}
saveAccounts();

// recvprob scale
$rscale = 0;
foreach ($accounts as $acc) $rscale += $acc['recvprob'];

$txlist = []; // this is the list of all transactions from the start
$txid = 0;
while (true) {
    $now = date('c');
    echo "Simulating transactions @ $now\n";

    foreach ($accounts as $from) {
        if (random() >= $from['sendprob']) continue;
        // select to account to send to
        $i = 0;
        $p = random() * $rscale;
        while ($i < $ACC_CNT && $p > $accounts[$i]['recvprob']) {
            $i++;
            $p -= $accounts[$i]['recvprob'];
        }
        if ($i >= $ACC_CNT) continue; // should not happen
        $to = $accounts[$i];
        $max = min($from['balance'], 100);
        if ($max == 0) continue; // no balance
        $amt = rand(1, $max);
        if ($amt > 95) $amt += 50 * ($amt-95);
        else if ($amt > 80) $amt += 10 * ($amt - 80);

        $txid++;
        $txlist[] = [
            "transaction_id" => $txid,
            "type" => 'transfer',
            "from" => $from['id'],
            "to" => $to['id'],
            "amount" => $amt,
            "description" => $desclist[mt_rand(0, count($desclist) - 1)],
            "timestamp" => date('c'),
            "signature" => "OK"
        ];
        echo "-- {$from['id']} => {$to['id']} - $amt\n";
    }

    file_put_contents("/tmp/transactions.json", json_encode($txlist));
    sleep(1);
}
