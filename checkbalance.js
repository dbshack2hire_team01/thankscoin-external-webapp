/**
 * Created by robymus on 3/26/17.
 */

$(function() {
    if (window.poolaccount) setInterval(function() {
       // check balance of public account every second
        $.ajax({
                type: 'GET',
                url: '//h.r2.io/ledger/account/'+window.poolaccount,
                success: function(data, status, xhr) {
                    window.updateAccountBalance(data['balance']);
                },
                error: function(xhr, status, response) {
                    console.log("error: "+status+"-"+response);
                    window.updateAccountBalance(0);
                }
            })
   }, 1000);
});