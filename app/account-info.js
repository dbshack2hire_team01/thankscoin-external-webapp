/*
 * A single account
 */
define(['knockout', 'app/model', 'koMapping'], function(ko, model, koMapping) {

    function AccountModel(params) {
        var _this = this;
        _this.data = params.data;
    }

    return { viewModel: AccountModel};

});

