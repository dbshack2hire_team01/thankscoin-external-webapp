requirejs.config({
    baseUrl: 'lib',
    paths: {
        app: '../app',
        koMapping: 'knockout.mapping'
    },
    shim: {
        bootstrap: { 'deps': ['jquery'] }
    }
})

requirejs(['app/init-ko'])