/*
 * Transaction list parsed and totals / display lists calculated
 */
define(['knockout', 'app/model', 'cytoscape', 'jquery', 'jquery-ui'], function(ko, model, cytoscape, $) {

    function objectSize(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    }

    function objectValues(obj) {
        var v = [], key;
        for (key in obj) {
            v.push(obj[key]);
        }
        return v;
    }

    function TransactionTotalsModel(params) {
        var _this = this;

        _this.apiUrl = '//h.r2.io/ledger/extransactions';
        _this.LAST_COUNT = 12; // show last X transactions
        _this.TX_WINDOW_SEC = [10, 60];
        _this.TOP_COUNT = 3;

        _this.lastTransactions = ko.observableArray();
        _this.txWindow = [[],[]]; // transactions in the last 5 seconds, 60 seconds, for different statistics
        _this.statRealtime = ko.mapping.fromJS({
            count: 0,
            volume: 0,
            active_credit: 0,
            active_debit: 0
        });
        _this.statWindow = [];
        _this.topTx = ko.observableArray();
        _this.topAccTxCnt = ko.observableArray();
        _this.topAccTxVol = ko.observableArray();
        _this.topAccCredit = ko.observableArray();
        _this.topAccDebit = ko.observableArray();

        _this.graphNodes = {};

        _this.last_tx_id = -1;
        _this.initialTxTime = new Date(new Date().getTime()-60000).toISOString();
        for (var i = 0; i < _this.TX_WINDOW_SEC.length; i++) {
            _this.statWindow[i] = ko.mapping.fromJS({
                count: 0, count_col: 0, count_icon: 0,
                volume: 0, volume_col: 0, volume_icon: 0,
                active_credit: 0, active_credit_col: 0, active_credit_icon: 0,
                active_debit: 0, active_debit_col: 0, active_debit_icon: 0,
                sec: 0
            });
        }

        var cy = cytoscape({
            container: $('#cy'),

            style: cytoscape.stylesheet()
                .selector('node')
                .css({
                    'shape': 'data(faveShape)',
                    'width': 'mapData(weight, 40, 80, 20, 60)',
                    'content': 'data(name)',
                    'text-valign': 'center',
                    'text-outline-width': 2,
                    'text-outline-color': 'data(faveColor)',
                    'background-color': 'data(faveColor)',
                    'color': '#fff'
                })
                .selector(':selected')
                .css({
                    'border-width': 3,
                    'border-color': '#333'
                })
                .selector('edge')
                .css({
                    'curve-style': 'bezier',
                    'opacity': 0.666,
                    'width': 'mapData(strength, 70, 100, 2, 6)',
                    'target-arrow-shape': 'triangle',
                    'source-arrow-shape': 'circle',
                    'line-color': 'data(faveColor)',
                    'source-arrow-color': 'data(faveColor)',
                    'target-arrow-color': 'data(faveColor)',
                    'label': 'data(label)',
                    'edge-text-rotation': 'autorotate'
                })
                .selector('edge.questionable')
                .css({
                    'line-style': 'dotted',
                    'target-arrow-shape': 'diamond'
                })
                .selector('.faded')
                .css({
                    'opacity': 0.25,
                    'text-opacity': 0
                })

        });

        _this.randomColor = function() {
            return ['#6FB1FC', '#EDA1ED', '#86B342', '#F5A45D'][Math.floor(Math.random()*4)];
        }

        _this.addGraphNode = function(nodeid, nodevalue) {
            nodeweight = nodevalue; // TODO: scale to 1..100??
            if (_this.graphNodes.hasOwnProperty(nodeid)) {
                // already in graph, update
                cy.nodes("[id='"+nodeid+"']").data({name: nodevalue, weight: nodeweight});
                _this.graphNodes[nodeid] ++;
            }
            else {
                // add new
                cy.add([
                    { data: { id: nodeid, name: nodevalue, weight: nodeweight, faveShape: 'ellipse', faveColor: _this.randomColor() } }
                ]);
                _this.graphNodes[nodeid] = 1;
            }
        }

        _this.delGraphNode = function(nodeid) {
            if (_this.graphNodes[nodeid] <= 1) {
                delete _this.graphNodes[nodeid];
                cy.remove("[id='"+nodeid+"']");
            }
            else {
                _this.graphNodes[nodeid]--;
            }
        }

        _this.addGraphEdge = function(from, from_amt, to, to_amt, edgeid, amt) {
            var weight = amt/50 + 10; // TODO: scale?
            _this.addGraphNode(from, from_amt);
            _this.addGraphNode(to, to_amt);
            cy.add([
                { data: { id: edgeid, source: from, target: to, label: amt, weight: weight, faveColor: _this.randomColor() } }
            ]);
        }

        _this.delGraphEdge = function(from, to, edgeid) {
            _this.delGraphNode(from);
            _this.delGraphNode(to);
            cy.remove("[id='"+edgeid+"']");
        }

        _this.smoothie1 = new SmoothieChart({
            grid: {
                strokeStyle: 'rgb(60, 60, 60)', fillStyle: 'rgb(20, 20, 20)',
                lineWidth: 1, millisPerLine: 250, verticalSections: 10,
            },
            labels: {fillStyle: 'rgb(255, 255, 80)'},
            minValue: 0
        });
        _this.smoothie1.streamTo(document.getElementById("chartCanvas1"), 1000);
        _this.line1 = new TimeSeries();
        _this.smoothie1.addTimeSeries(_this.line1,
            { strokeStyle:'rgb(0, 255, 0)', fillStyle:'rgba(0, 255, 0, 0.4)', lineWidth:3 });

        _this.smoothie2 = new SmoothieChart({
            grid: {
                strokeStyle: 'rgb(60, 60, 60)', fillStyle: 'rgb(20, 20, 20)',
                lineWidth: 1, millisPerLine: 250, verticalSections: 10,
            },
            labels: {fillStyle: 'rgb(255, 255, 80)'},
            minValue: 0
        });
        _this.smoothie2.streamTo(document.getElementById("chartCanvas2"), 1000);
        _this.line2 = new TimeSeries();
        _this.smoothie2.addTimeSeries(_this.line2,
            { strokeStyle:'rgb(0, 255, 255)', fillStyle:'rgba(0, 255, 255, 0.4)', lineWidth:3 });



        /*
         * This method adds the list of incoming transactions (polled every second) to statistics
         * -> update recent transactions
         * -> update realtime statistics
         * -> slide statistics window
         * -> recalculate stats for window
         * -> choose top txs and accounts from large window
         */
        _this.addTX = function(txlist) {

            // update recent transactions (and sliding window)
            var now = Math.floor(new Date().getTime() / 1000);
            var vol = 0;
            var cnt = 0;
            var act_credit = {};
            var act_debit = {};
            $.each(txlist, function(i, d0) {
                var d = d0[0];
                d['from'] = d['accountFrom'];
                d['to'] = d['accountTo'];
                d['transaction_id'] = d['id'];
                d['balance_from'] = d0[1];
                d['balance_to'] = d0[2];

                var tx = new model.Transaction(d);
                _this.lastTransactions.push( tx );

                act_debit[d['from']] = 1;
                act_credit[d['to']] = 1;

                d['addTime'] = Math.floor(new Date(d['timestamp']).getTime()/1000);
                for (var i = 0; i < _this.TX_WINDOW_SEC.length; i++) {
                    if (d['addTime']+_this.TX_WINDOW_SEC[i] > now) {
                        _this.txWindow[i].push( d );
                        if (i == 0) {
                            // add to graph
                            _this.addGraphEdge(d['from'], d['balance_from'], d['to'], d['balance_to'], d['transaction_id'], d['amount']);
                        }
                    }
                }
                cnt++;
                vol += d['amount'];

                _this.last_tx_id = d['transaction_id'];

            });
            while (_this.lastTransactions().length > _this.LAST_COUNT) {
                _this.lastTransactions.shift();
            }
            // update realtime statistics
            _this.statRealtime.count(cnt);
            _this.statRealtime.volume(vol);
            _this.statRealtime.active_credit(objectSize(act_credit));
            _this.statRealtime.active_debit(objectSize(act_debit));
            // slide statistics window
            for (var i = 0; i < _this.TX_WINDOW_SEC.length; i++) {
                var exp = _this.TX_WINDOW_SEC[i];
                while (_this.txWindow[i].length > 0 && _this.txWindow[i][0]['addTime'] + exp < now) {
                    var d = _this.txWindow[i].shift();
                    if (i == 0) {
                        // remove from graph
                        _this.delGraphEdge(d['from'], d['to'], d['transaction_id']);
                    }
                }
            }

            // relayout graph
            cy.layout({name:'circle', fit: true});

            // recalculate stats for window
            for (var i = 0; i < _this.TX_WINDOW_SEC.length; i++) _this.recalcWindowStats(i, now);

            _this.line1.append(new Date().getTime(), _this.statWindow[0].count());
            _this.line2.append(new Date().getTime(), _this.statWindow[0].volume());

            var txlist = _this.txWindow[1];

            // choose top TXs from window
            var toptx = txlist.sort(function(a,b) { return b['amount']-a['amount']} ).slice(0, _this.TOP_COUNT);
            _this.topTx( $.map(toptx, function(d) { return new model.Transaction(d); }) );

            // choose accounts from window
            _this.accountSummary = {}
            $.each(txlist, function(i, tx) {
                _this.addToSummary(tx['from'], -tx['amount']);
                _this.addToSummary(tx['to'], +tx['amount']);
            });
            txlist = objectValues(_this.accountSummary);

            var txcnt = txlist.sort(function(a,b) { var x = b['count']-a['count']; return x == 0 ? b['volume']-a['volume'] : x;} ).slice(0, _this.TOP_COUNT);
            _this.topAccTxCnt( $.map(txcnt, function(d) { return new model.AccountSummary(d, _this.statWindow[1].sec()); }) );

            var txvol = txlist.sort(function(a,b) { return b['volume']-a['volume']} ).slice(0, _this.TOP_COUNT);
            _this.topAccTxVol( $.map(txvol, function(d) { return new model.AccountSummary(d, _this.statWindow[1].sec()); }) );

            var credit = txlist.sort(function(a,b) { return b['credit']-a['credit']} ).slice(0, _this.TOP_COUNT);
            _this.topAccCredit( $.map(credit, function(d) { return new model.AccountSummary(d, _this.statWindow[1].sec()); }) );

            var debit = txlist.sort(function(a,b) { return b['debit']-a['debit']} ).slice(0, _this.TOP_COUNT);
            _this.topAccDebit( $.map(debit, function(d) { return new model.AccountSummary(d, _this.statWindow[1].sec()); }) );
        }

        _this.addToSummary = function(accid, amount) {
            if (_this.accountSummary.hasOwnProperty(accid)) {
                _this.accountSummary[accid].count ++;
                _this.accountSummary[accid].volume += Math.abs(amount);
                if (amount > 0) {
                    _this.accountSummary[accid].credit += amount;
                }
                else {
                    _this.accountSummary[accid].debit -= amount;
                }
            }
            else {
                _this.accountSummary[accid] = {
                    account_number: accid,
                    count: 1,
                    volume: Math.abs(amount),
                    credit: amount > 0 ? amount : 0,
                    debit: amount < 0 ? -amount : 0
                }
            }
        }

        _this.changeIcon = function(oldVal, newVal) {
            if (oldVal < newVal) return 'glyphicon-arrow-up';
            else if (oldVal > newVal) return 'glyphicon-arrow-down';
            else return 'glyphicon-minus';
        }

        _this.changeColor = function(oldVal, newVal) {
            if (oldVal < newVal) return 'col-up';
            else if (oldVal > newVal) return 'col-down';
            else return 'col-minus';
        }

        /**
         * Recalcultes statistics for window, putting results to stat
         */
        _this.recalcWindowStats = function(window_id, now) {
            var vol = 0;
            var cnt = 0;
            var act_credit = {};
            var act_debit = {};
            var startT = now-1;
            $.each(_this.txWindow[window_id], function(i, d) {
                startT = Math.min(startT,d['addTime']);
                act_debit[d['from']] = 1;
                act_credit[d['to']] = 1;
                cnt++;
                vol += d['amount'];
            });
            var sec = now-startT;
            var stat = _this.statWindow[window_id];
            cnt = Math.floor(cnt*100/sec)/100;
            vol = Math.floor(vol*100/sec)/100;
            var ac = objectSize(act_credit);
            var ad = objectSize(act_debit);
            stat.sec(sec);
            stat.count_col(_this.changeColor(stat.count(), cnt));
            stat.count_icon(_this.changeIcon(stat.count(), cnt));
            stat.count(cnt);
            stat.volume_col(_this.changeColor(stat.volume(), vol));
            stat.volume_icon(_this.changeIcon(stat.volume(), vol));
            stat.volume(vol);
            stat.active_credit_col(_this.changeColor(stat.active_credit(), ac));
            stat.active_credit_icon(_this.changeIcon(stat.active_credit(), ac));
            stat.active_credit(ac);
            stat.active_debit_col(_this.changeColor(stat.active_debit(), ad));
            stat.active_debit_icon(_this.changeIcon(stat.active_debit(), ad));
            stat.active_debit(ad);
        }

        _this.refreshList = function() {
            $.ajax({
                type: 'GET',
                url: _this.apiUrl,
                data: {
                    afterId: _this.last_tx_id,
                    txtimeFrom: _this.initialTxTime
                },
                error: function(xhr, status, response) {
                    console.log('error retrieving transaction list');
                    _this.error("Server error while retrieving account list, retrying soon.");
                },
                success: function(data, status, xhr) {
                    console.log('transaction list retrieved');

                    _this.addTX(data);
                }
            });
        };

        _this.error = ko.observable(null);

        window.setInterval(function() {
            _this.refreshList()
        }, 1000);

    }

    return { viewModel: TransactionTotalsModel};

});
