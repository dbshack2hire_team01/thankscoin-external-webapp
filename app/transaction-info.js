/*
 * A single account
 */
define(['knockout', 'app/model', 'koMapping'], function(ko, model, koMapping) {

    function TransactionModel(params) {
        var _this = this;
        _this.data = params.data;
        _this.data['timestamp_local'] = ko.observable(new Date(_this.data.timestamp()).toLocaleTimeString());
    }

    return { viewModel: TransactionModel};

});

