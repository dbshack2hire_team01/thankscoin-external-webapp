<?php

$ACC_CNT = 500;
$OPEN_PROB = 0.05;

$accid = [];

function random() {
    return mt_rand() / mt_getrandmax();
}

for ($i = 0; $i < $ACC_CNT; $i++) {
    $data = [
        "number" => md5("fake:".random()),
        "open" => random() < $OPEN_PROB
    ];

    $data_string = json_encode($data);
    $ch = curl_init('http://h.r2.io/ledger/account');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
    );

    $result = curl_exec($ch);

    if ($result === FALSE) {
        echo "err: {curl_errno($ch)}\n";
        continue;
    }
    else {
        echo "$result\n";
    }

    $xfer = [
        "accountFrom" => "corporate",
        "accountTo" => $data['number'],
        "amount" => 2000,
        "description" => "",
        "signature" => ""
    ];

    $data_string = json_encode($xfer);
    $ch = curl_init('http://h.r2.io/ledger/transaction');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
    );

    $result = curl_exec($ch);

    if ($result === FALSE) {
        echo "err: {curl_errno($ch)}\n";
        continue;
    }
    else {
        echo "$result\n";
    }

    $accid[] = $data['number'];

    echo "OK: ".$data['number']."\n";
}

file_put_contents("fakeaccounts.json", json_encode($accid));
